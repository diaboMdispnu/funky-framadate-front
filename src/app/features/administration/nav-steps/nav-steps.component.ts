import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PollService } from '../../../core/services/poll.service';
import { ToastService } from 'src/app/core/services/toast.service';

@Component({
	selector: 'app-nav-steps',
	templateUrl: './nav-steps.component.html',
	styleUrls: ['./nav-steps.component.scss'],
})
export class NavStepsComponent implements OnInit {
	@Input()
	display_next_button = true;
	@Input()
	previous_step_number = 1;
	@Input()
	next_step_number = 2;
	@Input()
	display_previous_button = true;
	@Input()
	is_finish_step = false;
	/**
	 * some action to perform before changing route
	 */
	@Input()
	actionBeforeNextStep: Function;
	@Input()
	next_is_disabled: any = false;

	constructor(private router: Router, private toastService: ToastService, public pollService: PollService) {}

	ngOnInit(): void {}

	/**
	 * launch custom function before next step
	 */
	runNextAction() {
		if (this.actionBeforeNextStep) {
			this.actionBeforeNextStep();
		}
		if (this.is_finish_step) {
			this.createPoll();
		}
	}

	createPoll() {
		this.pollService.createPoll().then(
			(resp) => {
				console.log('resp', resp);
				this.router.navigate(['/administration/success/']);
				this.pollService.step_current = null;
			},
			(err: any) => {
				console.error('oops err', err);
				this.toastService.display(err.message);
			}
		);
	}
}

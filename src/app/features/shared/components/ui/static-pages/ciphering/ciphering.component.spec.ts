import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CipheringComponent } from './ciphering.component';

describe('CipheringComponent', () => {
	let component: CipheringComponent;
	let fixture: ComponentFixture<CipheringComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CipheringComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CipheringComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

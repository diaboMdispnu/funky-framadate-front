import { Routes } from '@angular/router';
import { HomeComponent } from './core/components/home/home.component';
import { PollService } from './core/services/poll.service';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { SuccessComponent } from './features/administration/success/success.component';
import { WipTodoComponent } from './shared/components/ui/wip-todo/wip-todo.component';
import { CguComponent } from './features/shared/components/ui/static-pages/cgu/cgu.component';
import { LegalComponent } from './features/shared/components/ui/static-pages/legal/legal.component';
import { PrivacyComponent } from './features/shared/components/ui/static-pages/privacy/privacy.component';
import { CipheringComponent } from './features/shared/components/ui/static-pages/ciphering/ciphering.component';
import { AboutComponent } from './features/shared/components/ui/static-pages/about/about.component';
import { StaticPagesComponent } from './features/shared/static-pages/static-pages.component';

export const routes: Routes = [
	{ path: '', component: HomeComponent, data: { animation: 'HomePage' } },
	{
		path: 'user',
		loadChildren: () => import('./features/user-profile/user-profile.module').then((m) => m.UserProfileModule),
	},
	{
		path: 'administration',
		data: { animation: 'AdminPage' },
		loadChildren: () =>
			import('./features/administration/administration.module').then((m) => m.AdministrationModule),
	},
	{
		path: 'poll/:custom_url/consultation',
		loadChildren: () => import('./features/consultation/consultation.module').then((m) => m.ConsultationModule),
	},
	{
		path: 'success',
		component: SuccessComponent,
	},
	{
		path: 'todo',
		component: WipTodoComponent,
	},
	{
		path: 'page',
		component: StaticPagesComponent,
		children: [
			{
				path: 'cgu',
				component: CguComponent,
			},
			{
				path: 'legal',
				component: LegalComponent,
			},
			{
				path: 'privacy',
				component: PrivacyComponent,
			},
			{
				path: 'ciphering',
				component: CipheringComponent,
			},
			{
				path: 'about',
				component: AboutComponent,
			},
		],
	},

	{ path: 'page-not-found', component: PageNotFoundComponent },
	{ path: '**', redirectTo: 'page-not-found', pathMatch: 'full' },
];

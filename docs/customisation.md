# Personnalisation
Vous pouvez modifier les variables d'environnement afin de modifier le logo et le titre de votre installation.
Ce logiciel étant libre et sous la licence GNU Affero V3 vous pouvez bien entendu le modifier à volonté. Voici donc un guide concernant le titre et le logo.

Modifiez le fichier `src/environment.prod.ts`, et remplacez le contenu des variables `appTitle` (un texte, pas forcément un seul mot) et `appLogo` (une URL absolue ou relative d'image)
 Et voilà!
 
Quand vous builderez votre app, vous verrez ces valeurs dans la barre titre, incluses par le composant HeaderComponent automatiquement.

Vous pouvez modifier les valeurs dans le fichier  `src/environment.ts` pour voir ce que cela donne en environnement de développement en lancant l'app avec `npm start` ou `yarn start`.

#Customization
Change the content of the file `src/environment.prod.ts`, and the content of the vars `appTitle` (any text) and `appLogo` (any picture URL, relative or absolute) .
 And voilà!
